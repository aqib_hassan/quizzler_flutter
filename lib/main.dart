import 'package:flutter/material.dart';

void main() => runApp(Quizzler());

class Quizzler extends StatelessWidget {
  const Quizzler({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.grey.shade900,
        body: const SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.0),
            child: QuizPage(),
          ),
        ),
      ),
    );
  }
}

class QuizPage extends StatefulWidget {
  const QuizPage({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _QuizPageState createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  int currentQuestionNumber = 0;
  int points = 0;

  Icon checkIcon = const Icon(
    Icons.check,
    color: Colors.green,
  );

  Icon closeIcon = const Icon(
    Icons.close,
    color: Colors.red,
  );

  List<Icon> scoreKeeper = [];

  List questions = [
    {
      'question': 'You can lead a cow down stairs but not up stairs.',
      'answer': false
    },
    {
      'question': 'Approximately one quarter of human bones are in the feet.',
      'answer': true
    },
    {'question': 'A slug\'s blood is green.', 'answer': true},
  ];

  String getQuestion(int questionNumber) {
    return questions[questionNumber]['question'];
  }

  bool getAnswer(int questionNumber, bool answer) {
    bool correctAnswer = questions[questionNumber]['answer'];
    return correctAnswer == answer ? true : false;
  }

  int getPoint() {
    return points < 0 ? 0 : points;
  }

  void setCurrentQuestionNumber() {
    setState(() {
      currentQuestionNumber++;
    });
  }

  void setScoreKeeper(bool score) {
    setState(() {
      if (score == true) {
        scoreKeeper.add(checkIcon);
        points = points + 1;
      } else {
        scoreKeeper.add(closeIcon);
      }
    });
  }

  void setInitial() {
    setState(() {
      currentQuestionNumber = 0;
      points = 0;
      scoreKeeper = [];
    });
  }

  List<Widget> getWidget() {
    if (currentQuestionNumber == 3) {
      return [
        Expanded(
          flex: 5,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                'Your point is ${getPoint()}',
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(backgroundColor: Colors.green
                  // textStyle: TextStyle(
                  //   color: Colors.white,
                  // ),
                  ),
              child: const Text(
                'Start over',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                ),
              ),
              onPressed: () {
                setInitial();
              },
            ),
          ),
        ),
      ];
    } else {
      return [
        Expanded(
          flex: 5,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Text(
                //'This is where the question text will go.',
                getQuestion(currentQuestionNumber),
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(backgroundColor: Colors.green
                  // textStyle: TextStyle(
                  //   color: Colors.white,
                  // ),
                  ),
              child: const Text(
                'True',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20.0,
                ),
              ),
              onPressed: () {
                bool ans = getAnswer(currentQuestionNumber, true);
                setScoreKeeper(ans);
                setCurrentQuestionNumber();
              },
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(backgroundColor: Colors.red
                  // textStyle: TextStyle(
                  //   color: Colors.white,
                  // ),
                  ),
              child: const Text(
                'False',
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                bool ans = getAnswer(currentQuestionNumber, false);
                setScoreKeeper(ans);
                setCurrentQuestionNumber();
              },
            ),
          ),
        ),
        //TODO: Add a Row here as your score keeper
        Row(
          children: scoreKeeper,
        ),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: getWidget(),
    );
  }
}

/*
question1: 'You can lead a cow down stairs but not up stairs.', false,
question2: 'Approximately one quarter of human bones are in the feet.', true,
question3: 'A slug\'s blood is green.', true,
*/
